module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
			dist: {
				options: {},
				files: {
					"dist/index.js": [
						"src/mingos-markdown-py.js",
						"src/options.js",
						"src/convert.js",
						"src/index.js"
					]
				}
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");

	grunt.registerTask("default", ["concat:dist"]);
};
