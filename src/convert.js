/**
 * Run the md to html conversion
 * @param {String}   input    Either a file name or a string containing the markdown code
 * @param {Object}   options  Object containing the options to be passed to markdown_py
 * @param {Function} callback Callback function that will receive the output string as argument
 */
MingosMarkdownPy.convert = function(input, options, callback)
{
	// if there 2nd argument is missing, organise the arguments properly
	if (options instanceof Function) {
		callback = options;
		options = {};
	}

	var fs = require("fs"),
		childProcess = require('child_process'),
		chunks = [];

	// read the file contents, if possible.
	if (fs.existsSync(input)) {
		input = fs.readFileSync(input);
	}

	// spawn the markdown_py command as a child process
	var child = childProcess.spawn("markdown_py", prepareOptions(options));

	child.stdout.on("data", function(chunk) {
		chunks.push(chunk);
	});

	child.stderr.on("data", function (data) {
		console.log(data.toString());
	});

	child.on("exit", function() {
		var length = 0;
		chunks.forEach(function(chunk) {
			length += chunk.length;
		});
		var content = new Buffer(length);
		var index = 0;
		chunks.forEach(function(chunk) {
			chunk.copy(content, index, 0, chunk.length);
			index += chunk.length;
		});
		callback(content.toString());
	});

	child.stdin.write(input);
	child.stdin.end();
};
