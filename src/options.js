/**
 * Take in the options object and convert it to what the child process will understand
 * @param   {Object} options The options object. Use only full options names, e.g. if
 *                           markdown_py allows "-o html5" and "--output_format=html5",
 *                           always use the verbose version: {"output_format": "html5"}.
 * @returns {Array}          Options formatted as an array understandable to the child process
 */
var prepareOptions = function(options)
{
	var clone = require("clone"),
		output = [],
		key;

	// clone the options object (otherwise it will be modified since it's passed in by reference)
	options = clone(options);

	// extensions
	if (options.extension) {
		if (!(options.extension instanceof Array)) {
			options.extension = [options.extension]
		}
		options.extension.forEach(function(extension) {
			output.push("--extension=" + extension);
		});
		delete options.extension;
	}

	// change default for output format, if not set
	options.output_format = options.output_format || "html5";

	// build the output
	for (key in options) {
		if (options.hasOwnProperty(key)) {
			var value = options[key];
			if (null === value) {
				output.push("--" + key);
			} else {
				output.push("--" + key + "=" + value.toString());
			}
		}
	}

	return output;
};
