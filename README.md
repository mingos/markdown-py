# mingos-markdown-py

**mingos-markdown-py** is an npm module wrapping Python Markdown. It requires Python Markdown to be installed. To install it, make sure you have Python installed on your machine and issue the following command:

```
sudo pip install markdown
```

**mingos-markdown-py** will call the `markdown_py` command internally.

The idea behind wrapping the Python library in an NPM module is the ability to easily use it in the context of a Node.js script, e.g. a Grunt script. The difference between Python Markdown and markdown parsers available for JavaScript is the support for additional plugins, such as [Markdown Extra](https://michelf.ca/projects/php-markdown/extra/) or [Pygments](http://pygments.org/).

If an extension requires the use of another Python library (such as the aforementioned Pygments), it needs to be installed as well as the base markdown tool.

## Usage

The base usage is the following:

```
var md = require("mingos-markdown-py");
md.convert("path/to/file.md", function(output) {
    console.log(output);
});
```

Alternatively, the path to the source file can be replaced with a string containing the markdown source.

## Additional options

Options understandable by the `markdown_py` command are available. Only the verbose versions of the options are accepted, i.e. while `markdown_py` accepts `-x extra` and `--extension=extra`, the options object in **mingos-markdown-py** only allows the `extension` option.

```
var options = {
    extension: "extra",
    output_format: "xhtml"
};
md.convert("path/to/file.md", options, function(output) {
    console.log(output);
});
```

Multiple extensions can be specified by using an array of strings:

```
var options = {
    extension: [
        "extra",
        "smarty"
    ]
};
```

Extension config options must be passed within the string containing the extension name:

```
var options = {
    extension: [
        "extra",
        "smarty(smart_dashes=False,smart_quotes=False)"
    ]
};
```

Finally, options with no value are declared with `null`:

```
var options = {
    no_lazy_ol: null
};
```