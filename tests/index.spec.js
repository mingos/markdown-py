describe("mingos-markdown-py", function() {
	var md = require("../dist/index");

	it("should successfully produce the output HTML using default settings", function(done) {
		md.convert("tests/default.md", function(output) {
			expect(output).toBe("<h1>Lorem ipsum</h1>\n<hr>");
			done();
		});
	});

	it("should successfully apply extensions", function(done) {
		var options = {
			extension: [
				"extra", // without config
				"smarty(smart_quotes=false,smart_ellipses=True)" // with config
			]
		};
		md.convert("tests/extensions.md", options, function(output) {
			expect(output).toBe('<h1 class="heading">"Lorem" ipsum&hellip;</h1>');
			done();
		});
	});

	it("should apply other options", function(done) {
		var options = {
			output_format: "xhtml",
			no_lazy_ol: null
		};
		var expected = "<h1>Lorem ipsum</h1>\n" +
			"<hr />\n" +
			"<ol start=\"3\">\n" +
			"<li>Lorem</li>\n" +
			"<li>Ipsum</li>\n" +
			"<li>Dolor</li>\n" +
			"</ol>";
		md.convert("tests/options.md", options, function(output) {
			expect(output).toBe(expected);
			done();
		});
	});
});
